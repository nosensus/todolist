**IDE**
- Webstorm
- IntelliJ
- VisualCode

**Apps is Required**
- NodeJs (https://nodejs.org/en/)
- Ruby (https://rubyinstaller.org/)

**Data Base**
- Firebase from Google

**Init project**
- npm install
- bower install

**Start Project**
- gulp watch
**(if gulp watch not working try to install it global (npm install gulp -g))**

**Future functionality**
- use webpack 3 instead Gulp and Bower
- Card is seperate directive (component)
- Authorization
- Share to users
- telegramm_bot


