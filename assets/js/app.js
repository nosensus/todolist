var app = angular.module('myApp', ['ngRoute','firebase'])
    .config(function ($routeProvider, $locationProvider) {

        $locationProvider.hashPrefix('');

        $routeProvider
            .when("/today", {
                templateUrl: "assets/app/pages/today/today-template.html",
                controller: "todayController"
            })
            .when("/nextDay", {
                templateUrl: "assets/app/pages/nextday/next-day-template.html",
                controller: "nextDayController"
            })
            .when("/soon", {
                templateUrl: "assets/app/pages/soon/soon-template.html",
                controller: "soonController"
            })
            .otherwise({
                redirectTo: "/"
            });

    });

app.controller('mainController', function($scope){
    $scope.message = "Hello World"
});