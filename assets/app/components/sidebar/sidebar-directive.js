angular.module("myApp")
	.directive('sidebarComponent', function () {
	return {
		restrict: 'E',
		scope: {},
		//controller: "sidebarController as controller",
		templateUrl: 'assets/app/components/sidebar/sidebar-component.html'
	};
});
