app.controller('todayController', ['$scope', '$http', '$firebaseArray', todayCtrl]);

//.$inject = ['$scope', '$http', '$firebaseArray'];
function todayCtrl($scope, $http, $firebaseArray, todayCtrl) {
    $scope.fruits = [
        { id: '1', name: 'apple', isDone: true, isDeleted: false },
        { id: '2', name: 'banana', isDone: false, isDeleted: false },

    ]

    $scope.add = function () {
        $scope.editMode = true;
    }

    $scope.stopAdd = function () {
        $scope.editMode = false;
    }

    $scope.addToList = function (newFruitName) {

        for (var i = 0; i < $scope.fruits.length; i++) {
            if ($scope.fruits[i] == newFruitName) {
                alert('Duplicate');
                break;
            }
        }

        var id = $scope.fruits.length;
        var newFruit = { id: id, name: newFruitName, isDone: false, isDeleted: false };
        $scope.fruits.push(newFruit);
        $scope.newFruitName = '';

        var firebaseObj = new Firebase("https://todolisttima.firebaseio.com/");
        var products = $firebaseArray(firebaseObj);
        console.log($firebaseArray(firebaseObj));

        products.$add(newFruit);
    }

    $scope.editCurrentCard = function () {
        $scope.editMode = true;
    }

    $scope.saveCurrentCard = function () {
        $scope.editMode = false;
    }

    $scope.deleteCurrentCard = function () {
        $scope.fruits = [];
    }

    $scope.itemDelete = function (fruit) {
        // var findIndex = $scope.fruits.indexOf(fruit);
        // $scope.fruits.splice(findIndex, 1);
        fruit.isDeleted = true;
    }
}
