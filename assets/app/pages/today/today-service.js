/* можно создать через factory aor provider */

/* $http for POST and GET */

app.factory('todayService', ['$http', function($http){
    var path = "localhost"; //указать домен порт куда он должен стучаться в server.js
    var url = 'todoList/'; // тут мы разделяем данные по категориям, для удобства.
                          // это линк делает запрос на сервер под таким названием todoList может быть и cars и тд
    var service = {
        getTodoList: function (id){
            return $http.get(url + 'get' + id); // вернет с базы айди продукта
        },
        deleteTodoList: function (id){
            return $http.get(url + 'delete' + id); // вернет с базы айди продукта
        }
    };

    return service;
}]);