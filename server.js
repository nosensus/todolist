/*var http = require('http');
var server = new http.Server(); //http.Server -> net.Server -> EventEmitter

server.listen(1212, '127.0.0.1');

// подписка на событие
server.on('request', function(req, res){
    res.writeHead(200, {'Content-Type':'text/plain'}); //!**  text-plain означает чистый текст
    res.end("Hello world");
});*/

/* The sedond variant */
const http = require('http');

//также лучше всего указать статусы 200 и 404 а также какой тип данных мы передаем. Потому что это правильнее /!**
http.createServer( function(req, res){
    res.writeHead(200, {'Content-Type':'text/plain'}); //**  text-plain означает чистый текст
    res.end("Hello world"); // в строку мы можем подать и html только заменить text/html
}).listen(1212, '127.0.0.1');



/* необходимо подключиться к базе тут */
var sql = require("mssql");

var dbConfig = {
    server: "localhost\\SQL2K14",
    database: "TodoList",
    user:"",
    password: "",
    port: 1433
};

function getEmp(){

    var conn = new sql.Connection(dbConfig);
    var req = new sql.Request(conn);

    /*conn.connect(function(err){
        if(err) {
            console.log(err);
            return;
        }
        req.query("SELECT FROM ForToday", function(err, recordset) {
            if(err) {
                console.log(err);
            }
            else {
                console.log(recordset);
            }
            conn.close(); // закрывать коннекнш обязательно!
        })
    });*/

    /* variant TWO */
    conn.connect().then(function(){
        var req = new sql.Request(conn);
        req.query("SELECT * FROM ForToday").then(function(recordset){
            console.log(recordset);
            conn.clone();
        })
    })
    .catch(function(err){
        console.log(err);
        conn.clone();
    });
    conn.clone();
}

getEmp();